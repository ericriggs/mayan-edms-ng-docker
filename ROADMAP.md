Roadmap
=======
- Allow image to work with path volumes (only works with named volumes).
- Use a smaller base image than Ubuntu.
- Update to work with a recent Ubuntu version.
- Update to work with a recent Ubuntu version.
- Allow runtime setting of the uWSGI processes and threads numbers.
